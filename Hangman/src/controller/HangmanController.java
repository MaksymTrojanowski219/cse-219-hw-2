package controller;

import apptemplate.AppTemplate;
import data.GameData;
import gui.Workspace;
import javafx.animation.AnimationTimer;
import javafx.application.Platform;
import javafx.geometry.Pos;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import propertymanager.PropertyManager;
import ui.AppMessageDialogSingleton;
import ui.YesNoCancelDialogSingleton;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Random;
import java.util.Set;

import static settings.AppPropertyType.*;
import static settings.InitializationParameters.APP_WORKDIR_PATH;

/**
 * @author Ritwik Banerjee, Maksym Trojanowski
 */
public class HangmanController implements FileController {

    public enum GameState {
        UNINITIALIZED,
        INITIALIZED_UNMODIFIED,
        INITIALIZED_MODIFIED,
        ENDED
    }

    private AppTemplate appTemplate; // shared reference to the application
    private GameData    gamedata;    // shared reference to the game being played, loaded or saved
    private GameState   gamestate;   // the state of the game being shown in the workspace
    private Text[]      progress;    // reference to the text area for the word
    private boolean     success;     // whether or not player was successful
    private int         discovered;  // the number of letters already discovered
    private Button      gameButton;  // shared reference to the "start game" button
    private Label       remains;     // dynamically updated label that indicates the number of remaining guesses
    private Path        workFile;

    public HangmanController(AppTemplate appTemplate, Button gameButton) {
        this(appTemplate);
        this.gameButton = gameButton;
    }

    public HangmanController(AppTemplate appTemplate) {
        this.appTemplate = appTemplate;
        this.gamestate = GameState.UNINITIALIZED;
    }

    public void enableGameButton() {
        if (gameButton == null) {
            Workspace workspace = (Workspace) appTemplate.getWorkspaceComponent();
            gameButton = workspace.getStartGame();
        }
        gameButton.setDisable(false);
    }

    public void disableGameButton() {
        if (gameButton == null) {
            Workspace workspace = (Workspace) appTemplate.getWorkspaceComponent();
            gameButton = workspace.getStartGame();
        }
        gameButton.setDisable(true);
    }

    public void setGameState(GameState gamestate) {
        this.gamestate = gamestate;
    }

    public GameState getGamestate() {
        return this.gamestate;
    }

    /**
     * In the homework code given to you, we had the line
     * gamedata = new GameData(appTemplate, true);
     * This meant that the 'gamedata' variable had access to the app, but the data component of the app was still
     * the empty game data! What we need is to change this so that our 'gamedata' refers to the data component of
     * the app, instead of being a new object of type GameData. There are several ways of doing this. One of which
     * is to write (and use) the GameData#init() method.
     */
    public void start() {
        gamedata = (GameData) appTemplate.getDataComponent();
        success = false;
        discovered = 0;

        Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();

        gamedata.init();
        setGameState(GameState.INITIALIZED_UNMODIFIED);
        HBox remainingGuessBox = gameWorkspace.getRemainingGuessBox();
        HBox guessedLetters    = (HBox) gameWorkspace.getGameTextsPane().getChildren().get(1);
        //TODO get guessedBox
        FlowPane guessedBox    = (FlowPane) gameWorkspace.getGameTextsPane().getChildren().get(2);
        guessedBox.setPrefWrapLength(300);
        guessedBox.setPrefWidth(300);

        /////////////////////////////////////////////////////////////////////////////////////////
        //hintButton = (Button) gameWorkspace.getGameTextsPane().getChildren().get(3);
        //HBox hintBox = (HBox) gameWorkspace.getGameTextsPane().getChildren().get(3);
        Button hintButton = (Button)gameWorkspace.getGameTextsPane().getChildren().get(3);
        if (gamedata.getHintUsed())
        {
            hintButton.setDisable(true);
            gamedata.setHintUsed(true);
        }
        else
        {
            hintButton.setDisable(false);
            gamedata.setHintUsed(false);
        }

        if(getUnique()<8)
        {
            hintButton.setDisable(true);
            gamedata.setHintUsed(true);
        }

        //gameWorkspace.getGameTextsPane().getChildren().add(hintButton);
        hintButton.setOnMouseClicked(e -> handleHintRequest());


        remains = new Label(Integer.toString(GameData.TOTAL_NUMBER_OF_GUESSES_ALLOWED));
        remainingGuessBox.getChildren().addAll(new Label("Remaining Guesses: "), remains);
        //initWordGraphics(guessedLetters, guessedBox);
        initWordGraphics(guessedLetters, guessedBox, hintButton);
        play();
    }

    private void end() {
        appTemplate.getGUI().getPrimaryScene().setOnKeyTyped(null);
        gameButton.setDisable(true);
        Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
        Button hintButton = (Button)gameWorkspace.getGameTextsPane().getChildren().get(3);
        hintButton.setDisable(true);
        gamedata.setHintUsed(true);
        appTemplate.getGUI().updateWorkspaceToolbar(gamestate.equals(GameState.INITIALIZED_MODIFIED));
        Platform.runLater(() -> {
            PropertyManager           manager    = PropertyManager.getManager();
            AppMessageDialogSingleton dialog     = AppMessageDialogSingleton.getSingleton();
            String                    endMessage = manager.getPropertyValue(success ? GAME_WON_MESSAGE : GAME_LOST_MESSAGE);
            //if (!success)
            //    endMessage += String.format(" (the word was \"%s\")", gamedata.getTargetWord());
            if (dialog.isShowing())
                dialog.toFront();
            else
                dialog.show(manager.getPropertyValue(GAME_OVER_TITLE), endMessage);
        });
    }
//TODO DONE add squares instead
    private void initWordGraphics(HBox guessedLetters, FlowPane guessedBox, Button hintButton) {
        char[] targetword = gamedata.getTargetWord().toCharArray();
        Rectangle[] rectangles = new Rectangle[targetword.length];
        StackPane[] stacks = new StackPane[targetword.length];
        progress = new Text[targetword.length];
        for (int i = 0; i < progress.length; i++) {
            rectangles[i] = new Rectangle(20,20,20,20);
            rectangles[i].setFill(Paint.valueOf("#ffffff"));
            rectangles[i].setStroke(Paint.valueOf("#000000"));
            progress[i] = new Text(Character.toString(targetword[i]));
            progress[i].setVisible(false);
            StackPane stack = new StackPane();
            stack.getChildren().add(rectangles[i]);
            stack.getChildren().add(progress[i]);
            stacks[i] = stack;
        }
        //guessedLetters.getChildren().addAll(progress);
        guessedLetters.getChildren().addAll(stacks);
        //guessedBox.getChildren().addAll(stacks);


      //TODO get guessedBox children
      Rectangle[] guessedRectangles = new Rectangle[26];
      StackPane[] guessedStacks = new StackPane[26];
      Text[] letters = {new Text("a"), new Text("b"), new Text("c"), new Text("d"), new Text("e"), new Text("f"),
                        new Text("g"), new Text("h"), new Text("i"), new Text("j"), new Text("k"), new Text("l"), new Text("m"),
                        new Text("n"), new Text("o"), new Text("p"), new Text("q"), new Text("r"), new Text("s"), new Text("t"),
                        new Text("u"), new Text("v"), new Text("w"), new Text("x"), new Text("y"), new Text("z")};
      for (int i = 0; i < 26; i++)
      {
          guessedRectangles[i] = new Rectangle(50,50,50,50);
          guessedRectangles[i].setFill(Paint.valueOf("#00ff00"));
          guessedRectangles[i].setStroke(Paint.valueOf("#000000"));
          StackPane guessStack = new StackPane();
          guessStack.getChildren().add(guessedRectangles[i]);
          guessStack.getChildren().add(letters[i]);
          guessedStacks[i] = guessStack;
      }

      guessedBox.getChildren().addAll(guessedStacks);

        hintButton.setOnMouseClicked(e -> handleHintRequest());
        hintButton.setVisible(true);
        if (gamedata.getHintUsed())
        {
            hintButton.setDisable(true);
            gamedata.setHintUsed(true);
        }
        else
        {
            hintButton.setDisable(false);
            gamedata.setHintUsed(false);
        }
    }

    public void play() {
        Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();

        disableGameButton();
        AnimationTimer timer = new AnimationTimer() {
            @Override
            public void handle(long now) {
                appTemplate.getGUI().updateWorkspaceToolbar(gamestate.equals(GameState.INITIALIZED_MODIFIED));
                appTemplate.getGUI().getPrimaryScene().setOnKeyTyped((KeyEvent event) -> {
                    char guess = event.getCharacter().charAt(0);
                    int guessNum = (int)guess;
                    if (guessNum > 122 || guessNum <97)
                    {System.out.println("Invalid char(" + guess + ")");}
                    else if (!alreadyGuessed(guess)) {
                        boolean goodguess = false;
                        for (int i = 0; i < progress.length; i++) {
                            if (gamedata.getTargetWord().charAt(i) == guess) {
                                progress[i].setVisible(true);
                                gamedata.addGoodGuess(guess);
                                goodguess = true;
                                discovered++;
                            }
                        }
                        if (!goodguess)
                        {
                            gamedata.addBadGuess(guess);
                            renderHangman(gamedata.getRemainingGuesses());
                        }

                        success = (discovered == progress.length);
                        remains.setText(Integer.toString(gamedata.getRemainingGuesses()));
                        StackPane tempPane = (StackPane) gameWorkspace.getGuessedBox().getChildren().get(guessNum - 97);
                        Rectangle tempRect = (Rectangle) tempPane.getChildren().get(0);
                        tempRect.setFill(Paint.valueOf("#009900"));
                        setGameState(GameState.INITIALIZED_MODIFIED);
                    }

                });
                if (gamedata.getRemainingGuesses() <= 0 || success)
                    stop();
            }

            @Override
            public void stop() {

                HBox guessedLetters    = (HBox) gameWorkspace.getGameTextsPane().getChildren().get(1);
                for(int i = 0; i < progress.length; i++)
                {
                    if(!progress[i].visibleProperty().get())
                    {
                        StackPane temp = (StackPane)guessedLetters.getChildren().get(i);
                        Rectangle temprec = (Rectangle) temp.getChildren().get(0);
                        Text temptext = (Text) temp.getChildren().get(1);
                        temptext.setVisible(true);
                        temprec.setFill(Paint.valueOf("#ff0000"));
                    }

                }
                super.stop();
                end();

            }
        };
        timer.start();
    }

    public void renderHangman(int x)
    {
        Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
        Canvas canvas = (Canvas) gameWorkspace.getFigurePane().getChildren().get(0);
        GraphicsContext gc = canvas.getGraphicsContext2D();
        gc.setStroke(Paint.valueOf("#000000"));
        gc.setLineWidth(5);

        switch(x) {
            case 9:
                gc.moveTo(0,300);
                gc.lineTo(300,300);
                gc.stroke();
                break;
            case 8:
                gc.moveTo(0,300);
                gc.lineTo(0,0);
                gc.stroke();
                break;
            case 7:
                gc.moveTo(0,0);
                gc.lineTo(150,0);
                gc.stroke();
                break;
            case 6:
                gc.moveTo(150,0);
                gc.lineTo(150,50);
                gc.stroke();
                break;
            case 5:
                gc.moveTo(150,50);
                gc.fillOval(125,50,50,50);
                gc.stroke();
                break;
            case 4:
                gc.moveTo(150,0);
                gc.lineTo(150,200);
                gc.stroke();
                break;
            case 3:
                gc.moveTo(150,200);
                gc.lineTo(125,225);
                gc.stroke();
                break;
            case 2:
                gc.moveTo(150,200);
                gc.lineTo(175,225);
                gc.stroke();
                break;
            case 1:
                gc.moveTo(150,125);
                gc.lineTo(125,175);
                gc.stroke();
                break;
            case 0:
                gc.moveTo(150,125);
                gc.lineTo(175,175);
                gc.stroke();
                break;

        }
    }


    private void restoreGUI() {
        disableGameButton();
        Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
        gameWorkspace.reinitialize();

        HBox guessedLetters = (HBox) gameWorkspace.getGameTextsPane().getChildren().get(1);
        FlowPane guessedBox    = (FlowPane) gameWorkspace.getGameTextsPane().getChildren().get(2);
        Button hintButton = (Button) gameWorkspace.getGameTextsPane().getChildren().get(3);

        guessedBox.setPrefWrapLength(300);
        guessedBox.setPrefWidth(300);
        restoreWordGraphics(guessedLetters, guessedBox);

        HBox remainingGuessBox = gameWorkspace.getRemainingGuessBox();
        remains = new Label(Integer.toString(gamedata.getRemainingGuesses()));
        remainingGuessBox.getChildren().addAll(new Label("Remaining Guesses: "), remains);

        hintButton.setOnMouseClicked(e -> handleHintRequest());
        hintButton.setVisible(true);
        if(gamedata.getHintUsed())
        {
            hintButton.setDisable(true);
            gamedata.setHintUsed(true);
        }

        else
        {
            hintButton.setDisable(false);
            gamedata.setHintUsed(false);
        }


        success = false;
        play();
    }

    private void restoreWordGraphics(HBox guessedLetters, FlowPane guessedBox) {
        discovered = 0;
        char[] targetword = gamedata.getTargetWord().toCharArray();
        Rectangle[] rectangles = new Rectangle[targetword.length];
        StackPane[] stacks = new StackPane[targetword.length];
        progress = new Text[targetword.length];
        for (int i = 0; i < progress.length; i++) {
            rectangles[i] = new Rectangle(20,20,20,20);
            rectangles[i].setFill(Paint.valueOf("#ffffff"));
            rectangles[i].setStroke(Paint.valueOf("#000000"));
            progress[i] = new Text(Character.toString(targetword[i]));
            progress[i].setVisible(gamedata.getGoodGuesses().contains(progress[i].getText().charAt(0)));
            if (progress[i].isVisible())
                discovered++;
            StackPane stack = new StackPane();
            stack.getChildren().add(rectangles[i]);
            stack.getChildren().add(progress[i]);
            stacks[i] = stack;
        }
        //guessedLetters.getChildren().addAll(progress);
        guessedLetters.getChildren().addAll(stacks);

        Rectangle[] guessedRectangles = new Rectangle[26];
        StackPane[] guessedStacks = new StackPane[26];
        Text[] letters = {new Text("a"), new Text("b"), new Text("c"), new Text("d"), new Text("e"), new Text("f"),
                new Text("g"), new Text("h"), new Text("i"), new Text("j"), new Text("k"), new Text("l"), new Text("m"),
                new Text("n"), new Text("o"), new Text("p"), new Text("q"), new Text("r"), new Text("s"), new Text("t"),
                new Text("u"), new Text("v"), new Text("w"), new Text("x"), new Text("y"), new Text("z")};
        for (int i = 0; i < 26; i++)
        {
            guessedRectangles[i] = new Rectangle(50,50,50,50);
            guessedRectangles[i].setFill(Paint.valueOf("#00ff00"));
            guessedRectangles[i].setStroke(Paint.valueOf("#000000"));
            StackPane guessStack = new StackPane();
            guessStack.getChildren().add(guessedRectangles[i]);
            guessStack.getChildren().add(letters[i]);
            guessedStacks[i] = guessStack;
        }

        guessedBox.getChildren().addAll(guessedStacks);

        Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
        Object[] badSet = gamedata.getBadGuesses().toArray();
        Object[] goodSet = gamedata.getGoodGuesses().toArray();
        for(int i = 0; i < badSet.length; i++)
        {
            StackPane tempPane = (StackPane) gameWorkspace.getGuessedBox().getChildren().get((int)(char)badSet[i] - 97);
            Rectangle tempRect = (Rectangle) tempPane.getChildren().get(0);
            tempRect.setFill(Paint.valueOf("#009900"));
        }
        for(int i = 0; i < goodSet.length; i++)
        {
            StackPane tempPane = (StackPane) gameWorkspace.getGuessedBox().getChildren().get((int)(char)goodSet[i] - 97);
            Rectangle tempRect = (Rectangle) tempPane.getChildren().get(0);
            tempRect.setFill(Paint.valueOf("#009900"));
        }

        for(int i = 10; i >= gamedata.getRemainingGuesses(); i--)
        {
            renderHangman(i);
        }

        Button hintButton = (Button)gameWorkspace.getGameTextsPane().getChildren().get(3);
        hintButton.setOnMouseClicked(e -> handleHintRequest());
        hintButton.setVisible(true);
        if(gamedata.getHintUsed())
        {
            hintButton.setDisable(true);
            gamedata.setHintUsed(true);
        }

        else
        {
            hintButton.setDisable(false);
            gamedata.setHintUsed(false);
        }


    }

    private boolean alreadyGuessed(char c) {
        return gamedata.getGoodGuesses().contains(c) || gamedata.getBadGuesses().contains(c);
    }

    public void handleHintRequest()
    {
        Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
        Button hintButton = (Button)gameWorkspace.getGameTextsPane().getChildren().get(3);
        hintButton.setDisable(true);
        gamedata.setHintUsed(true);
        String word = gamedata.getTargetWord();
        Object[] goodSet = gamedata.getGoodGuesses().toArray();
        Object[] badSet = gamedata.getBadGuesses().toArray();
        char[] used = new char[26];
        //System.out.print(total + "hello");
        for(int i = 0; i < goodSet.length; i++)
        {
            used[i] = (char)goodSet[i];
            //System.out.print(used[i]);
        }
        char[] available = new char[26];
        int total = 0;

       // System.out.print(word.length());
        for(int i = 0; i < word.length(); i++)
        {

            for(int j = 0; j < used.length; j++)
            {
                if(word.charAt(i) == used[j]) //if bad char
                {
                    j = used.length; //quit
                }
                else if(j == used.length-1 && word.charAt(i) != used[j])
                {
                    available[total] = word.charAt(i);
                    //System.out.print(available[total]);
                    total++;
                }
            }
        }

        //pick random from available
        Random rand = new Random();
        int y = rand.nextInt(total);
        System.out.print(y);
        char guess = available[y];
        int guessNum = (int)guess;

        for (int i = 0; i < progress.length; i++) {
            if (gamedata.getTargetWord().charAt(i) == guess) {
                progress[i].setVisible(true);
                gamedata.addGoodGuess(guess);
            }
        }



        gamedata.addBadGuess();
        remains.setText(Integer.toString(gamedata.getRemainingGuesses()));
        renderHangman(gamedata.getRemainingGuesses());
        StackPane tempPane = (StackPane) gameWorkspace.getGuessedBox().getChildren().get(guessNum - 97);
        Rectangle tempRect = (Rectangle) tempPane.getChildren().get(0);
        tempRect.setFill(Paint.valueOf("#009900"));
        setGameState(GameState.INITIALIZED_MODIFIED);

    }

//    char guess = event.getCharacter().charAt(0);
//    int guessNum = (int)guess;
//                    if (guessNum > 122 || guessNum <97)
//    {System.out.println("Invalid char(" + guess + ")");}
//                    else if (!alreadyGuessed(guess)) {
//        boolean goodguess = false;
//        for (int i = 0; i < progress.length; i++) {
//            if (gamedata.getTargetWord().charAt(i) == guess) {
//                progress[i].setVisible(true);
//                gamedata.addGoodGuess(guess);
//                goodguess = true;
//                discovered++;
//            }
//        }
//        if (!goodguess)
//        {
//            gamedata.addBadGuess(guess);
//            renderHangman(gamedata.getRemainingGuesses());
//        }
//
//        success = (discovered == progress.length);
//        remains.setText(Integer.toString(gamedata.getRemainingGuesses()));
//        StackPane tempPane = (StackPane) gameWorkspace.getGuessedBox().getChildren().get(guessNum - 97);
//        Rectangle tempRect = (Rectangle) tempPane.getChildren().get(0);
//        tempRect.setFill(Paint.valueOf("#009900"));
//        setGameState(GameState.INITIALIZED_MODIFIED);
//    }

    @Override
    public void handleNewRequest() {
        AppMessageDialogSingleton messageDialog   = AppMessageDialogSingleton.getSingleton();
        PropertyManager           propertyManager = PropertyManager.getManager();
        boolean                   makenew         = true;
        if (gamestate.equals(GameState.INITIALIZED_MODIFIED))
            try {
                makenew = promptToSave();
            } catch (IOException e) {
                messageDialog.show(propertyManager.getPropertyValue(NEW_ERROR_TITLE), propertyManager.getPropertyValue(NEW_ERROR_MESSAGE));
            }
        if (makenew) {
            appTemplate.getDataComponent().reset();                // reset the data (should be reflected in GUI)
            appTemplate.getWorkspaceComponent().reloadWorkspace(); // load data into workspace
            ensureActivatedWorkspace();                            // ensure workspace is activated
            workFile = null;                                       // new workspace has never been saved to a file
            ((Workspace) appTemplate.getWorkspaceComponent()).reinitialize();
            enableGameButton();
        }
        if (gamestate.equals(GameState.ENDED)) {
            appTemplate.getGUI().updateWorkspaceToolbar(false);
            Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
            gameWorkspace.reinitialize();
        }

    }

    @Override
    public void handleSaveRequest() throws IOException {
        PropertyManager propertyManager = PropertyManager.getManager();
        if (workFile == null) {
            FileChooser filechooser = new FileChooser();
            Path        appDirPath  = Paths.get(propertyManager.getPropertyValue(APP_TITLE)).toAbsolutePath();
            Path        targetPath  = appDirPath.resolve(APP_WORKDIR_PATH.getParameter());
            filechooser.setInitialDirectory(targetPath.toFile());
            filechooser.setTitle(propertyManager.getPropertyValue(SAVE_WORK_TITLE));
            String description = propertyManager.getPropertyValue(WORK_FILE_EXT_DESC);
            String extension   = propertyManager.getPropertyValue(WORK_FILE_EXT);
            ExtensionFilter extFilter = new ExtensionFilter(String.format("%s (*.%s)", description, extension),
                    String.format("*.%s", extension));
            filechooser.getExtensionFilters().add(extFilter);
            File selectedFile = filechooser.showSaveDialog(appTemplate.getGUI().getWindow());
            if (selectedFile != null)
                save(selectedFile.toPath());
        } else
            save(workFile);
    }

    @Override
    public void handleLoadRequest() throws IOException {
        boolean load = true;
        if (gamestate.equals(GameState.INITIALIZED_MODIFIED))
            load = promptToSave();
        if (load) {
            PropertyManager propertyManager = PropertyManager.getManager();
            FileChooser     filechooser     = new FileChooser();
            Path            appDirPath      = Paths.get(propertyManager.getPropertyValue(APP_TITLE)).toAbsolutePath();
            Path            targetPath      = appDirPath.resolve(APP_WORKDIR_PATH.getParameter());
            filechooser.setInitialDirectory(targetPath.toFile());
            filechooser.setTitle(propertyManager.getPropertyValue(LOAD_WORK_TITLE));
            String description = propertyManager.getPropertyValue(WORK_FILE_EXT_DESC);
            String extension   = propertyManager.getPropertyValue(WORK_FILE_EXT);
            ExtensionFilter extFilter = new ExtensionFilter(String.format("%s (*.%s)", description, extension),
                    String.format("*.%s", extension));
            filechooser.getExtensionFilters().add(extFilter);
            File selectedFile = filechooser.showOpenDialog(appTemplate.getGUI().getWindow());
            if (selectedFile != null && selectedFile.exists())
                load(selectedFile.toPath());
            restoreGUI(); // restores the GUI to reflect the state in which the loaded game was last saved
        }
    }

    @Override
    public void handleExitRequest() {
        try {
            boolean exit = true;
            if (gamestate.equals(GameState.INITIALIZED_MODIFIED))
                exit = promptToSave();
            if (exit)
                System.exit(0);
        } catch (IOException ioe) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            PropertyManager           props  = PropertyManager.getManager();
            dialog.show(props.getPropertyValue(SAVE_ERROR_TITLE), props.getPropertyValue(SAVE_ERROR_MESSAGE));
        }
    }

    private void ensureActivatedWorkspace() {
        appTemplate.getWorkspaceComponent().activateWorkspace(appTemplate.getGUI().getAppPane());
    }

    private boolean promptToSave() throws IOException {
        PropertyManager            propertyManager   = PropertyManager.getManager();
        YesNoCancelDialogSingleton yesNoCancelDialog = YesNoCancelDialogSingleton.getSingleton();

        yesNoCancelDialog.show(propertyManager.getPropertyValue(SAVE_UNSAVED_WORK_TITLE),
                propertyManager.getPropertyValue(SAVE_UNSAVED_WORK_MESSAGE));

        if (yesNoCancelDialog.getSelection().equals(YesNoCancelDialogSingleton.YES))
            handleSaveRequest();

        return !yesNoCancelDialog.getSelection().equals(YesNoCancelDialogSingleton.CANCEL);
    }

    /**
     * A helper method to save work. It saves the work, marks the current work file as saved, notifies the user, and
     * updates the appropriate controls in the user interface
     *
     * @param target The file to which the work will be saved.
     * @throws IOException
     */
    private void save(Path target) throws IOException {
        appTemplate.getFileComponent().saveData(appTemplate.getDataComponent(), target);
        workFile = target;
        setGameState(GameState.INITIALIZED_UNMODIFIED);
        AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
        PropertyManager           props  = PropertyManager.getManager();
        dialog.show(props.getPropertyValue(SAVE_COMPLETED_TITLE), props.getPropertyValue(SAVE_COMPLETED_MESSAGE));
    }

    /**
     * A helper method to load saved game data. It loads the game data, notified the user, and then updates the GUI to
     * reflect the correct state of the game.
     *
     * @param source The source data file from which the game is loaded.
     * @throws IOException
     */
    private void load(Path source) throws IOException {
        // load game data
        appTemplate.getFileComponent().loadData(appTemplate.getDataComponent(), source);

        // set the work file as the file from which the game was loaded
        workFile = source;

        // notify the user that load was successful
        AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
        PropertyManager           props  = PropertyManager.getManager();
        dialog.show(props.getPropertyValue(LOAD_COMPLETED_TITLE), props.getPropertyValue(LOAD_COMPLETED_MESSAGE));

        setGameState(GameState.INITIALIZED_UNMODIFIED);
        Workspace gameworkspace = (Workspace) appTemplate.getWorkspaceComponent();
        ensureActivatedWorkspace();
        gameworkspace.reinitialize();
        gamedata = (GameData) appTemplate.getDataComponent();
    }

    public int getUnique()
    {
        String word = gamedata.getTargetWord();
        int unique = 0;
        String let = "abcdefghijklmnopqrstuvwxyz";
        String used = "";
        for(int i = 0; i <word.length(); i++)
        {
            if (let.contains(String.valueOf(word.charAt(i))) && used.contains(String.valueOf(word.charAt(i))) == false)
            {
                unique++;
                used += word.charAt(i);
            }
        }
        return unique;
    }
}
